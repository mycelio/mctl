package get

import (
	"context"
	"errors"
	"fmt"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"google.golang.org/grpc"
	"mctl/client"
)

type Options struct {
}

//type VersionOutput struct {
//	Myceliod string `json:"myceliod"`
//	Mctl string `json:"mctl"`
//}

func NewCmd(o Options) *cobra.Command {

	cmd := &cobra.Command{
		Use:   "get",
		Short: "Get resource",
		Run: func(cmd *cobra.Command, args []string) {
			o.Run(cmd.Context(), cmd, args)
		},
	}

	return cmd
}

func (o *Options) Run(ctx context.Context, cmd *cobra.Command, args []string) {
	c := client.NewClient(client.Options{
		Ctx: ctx,
		Address: viper.GetString("client.address"),
		DialOptions: []grpc.DialOption{grpc.WithInsecure(), grpc.WithBlock()},
	})

	_, err := c.Connect()
	cobra.CheckErr(err)

	defer c.Disconnect()

	// TODO: Add helper
	if (len(args) == 2) {
		resource := args[0]
		name := args[1]

		b, err := c.Get(resource, name)

		cobra.CheckErr(err)
		fmt.Println(string(b))
	} else {
		err = errors.New("insufficient arguments")
		cobra.CheckErr(err)
	}
}

