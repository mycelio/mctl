package version

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"google.golang.org/grpc"
	"mctl/client"
)

type Options struct {
}

type VersionOutput struct {
	Myceliod string `json:"myceliod"`
	Mctl string `json:"mctl"`
}

func NewCmd(o Options) *cobra.Command {

	cmd := &cobra.Command{
		Use:   "version",
		Short: "Get server version",
		Run: func(cmd *cobra.Command, args []string) {
			o.Run(cmd.Context(), cmd, args)
		},
	}

	return cmd
}

func (o *Options) Run(ctx context.Context, cmd *cobra.Command, args []string) {
	c := client.NewClient(client.Options{
		Ctx: ctx,
		Address: viper.GetString("client.address"),
		DialOptions: []grpc.DialOption{grpc.WithInsecure(), grpc.WithBlock()},
	})

	_, err := c.Connect()
	cobra.CheckErr(err)

	defer c.Disconnect()

	version, err := c.GetVersion()

	versionOutput := VersionOutput{
		Myceliod: version,
		Mctl: "unknown",
	}

	b, err := json.MarshalIndent(versionOutput, "", "  ")
	cobra.CheckErr(err)

	fmt.Println(string(b))
}
