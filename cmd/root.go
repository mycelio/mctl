package cmd

import (
	"context"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/mycelio/cmdutil/config"
	"gitlab.com/mycelio/cmdutil/logger"
	"mctl/cmd/get"
	"mctl/cmd/version"
)

type Options struct {
}


func NewCmd(options Options) *cobra.Command {
	cmd := &cobra.Command{
		Use:   "mctl",
		Short: "mctl controls myceliod server",
		PersistentPreRun: func(cmd *cobra.Command, args []string) {
			initConfig()
			initLogger()

			logger.Debug("config", viper.ConfigFileUsed())
		},
	}

	// Flags
	cmd.PersistentFlags().String("config", "", "Config file")
	cmd.PersistentFlags().CountP("verbose", "v", "Verbosity level")
	cmd.PersistentFlags().String("address", ":5000", "Mycelio server address")

	viper.BindPFlag("logging.verbosity", cmd.PersistentFlags().Lookup("verbose"))
	viper.BindPFlag("config", cmd.PersistentFlags().Lookup("config"))
	viper.BindPFlag("client.address", cmd.PersistentFlags().Lookup("address"))


	// Commands
	cmd.AddCommand(version.NewCmd(version.Options{}))
	cmd.AddCommand(get.NewCmd(get.Options{}))

	return cmd
}

func (o *Options) Run(ctx context.Context, cmd *cobra.Command, args []string) {

}

func Execute() {
	rootCmd := NewCmd(Options{})

	err := rootCmd.Execute()
	cobra.CheckErr(err)
}

func initLogger() {
	logger.NewLogger(logger.Options{
		Verbosity: viper.GetInt("logging.verbosity"),
	})
}

func initConfig() {
	config.Configure(config.Options{
		Path: viper.GetString("config"),
		DefaultFileName: ".mctl",
	})
}
