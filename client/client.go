package client

import (
	"context"
	"gitlab.com/mycelio/cmdutil/logger"
	pb "gitlab.com/mycelio/myceliod/mycelio"
	"google.golang.org/grpc"
)

type client struct {
	ctx context.Context
	address string
	dialOptions []grpc.DialOption
	connection *grpc.ClientConn
	client pb.MycelioClient
}

type Options struct {
	Ctx context.Context
	Address string
	DialOptions []grpc.DialOption
}

func NewClient(options Options) *client {
	if options.Ctx == nil {
		options.Ctx = context.Background()
	}

	c := client{
		ctx: options.Ctx,
		address: options.Address,
		dialOptions: options.DialOptions,
	}

	return &c
}

func (c *client) Connect() (*pb.MycelioClient, error)  {
	var err error

	logger.Debug("msg", "Connecting to myceliod", "address", c.address)

	c.connection, err = grpc.DialContext(c.ctx, c.address, c.dialOptions...)
	if err != nil {
		return nil, err
	}

	logger.Debug("msg", "Connected to myceliod", "address", c.address)

	c.client = pb.NewMycelioClient(c.connection)

	return &c.client, nil
}

func (c *client) Disconnect() error {
	err := c.connection.Close()
	logger.Debug("msg", "Connection to myceliod closed", "address", c.address)

	return err
}



