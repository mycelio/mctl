package client

import (
	"context"
	"fmt"
	pb "gitlab.com/mycelio/myceliod/mycelio"
	"time"
)

func (c *client) GetVersion() (string, error) {
	ctx, cancel := context.WithTimeout(c.ctx, time.Second)
	defer cancel()

	r, err := c.client.GetVersion(ctx, &pb.GetVersionRequest{})

	return fmt.Sprintf("%s.%s.%s", r.Version.Major, r.Version.Minor, r.Version.Patch), err
}
