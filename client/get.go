package client

import (
	"context"
	pb "gitlab.com/mycelio/myceliod/mycelio"
	"time"
)

func (c *client) Get(resource string, name string) ([]byte, error) {
	ctx, cancel := context.WithTimeout(c.ctx, time.Second)
	defer cancel()

	r, err := c.client.Get(ctx, &pb.GetRequest{Resource: resource, Name: name})
	if err != nil {
		return nil, err
	}

	return r.Payload, nil
}
